/*
PROGRAM INI HANYA DIGUNAKAN UNTUK 1 NEURON OUTPUT SAJA
*/

const int InputNodes = 2;
const int HiddenNodes = 10;
const int OutputNodes = 1;
const int ADCMin = 0;
const int ADCMax = 4095;
const float InputMax = 100.0;
const float InputMin = 0.0;

const float _w1[HiddenNodes][InputNodes] = {
  {-0.7084217,-0.9461509},
  {-2.0699844,-1.6427511},
  {-3.5386367,-3.5317478},
  {-1.4538662,-1.0077903},
  {-4.5922093,-4.776662},
  {1.9746007,1.6351537},
  {-2.7225,-2.0037186},
  {-2.5639892,-2.5942168},
  {6.747238,6.6512876},
  {1.1316481,1.0396574}
};

const float _b1[] = {
  -0.9145845,
  -0.055001795,
  0.9668325,
  -0.6728337,
  1.4258794,
  -0.45360643,
  0.35587642,
  0.49153203,
  -2.1631718,
  -0.6261259
};

const float _w2[] = {
  0.93281657,
  2.4632075,
  4.8575864,
  1.540151,
  6.4997945,
  -2.7283416,
  3.1627886,
  3.507378,
  -10.25883,
  -1.7072216
};

const float _b2 = -1.6090062;

float _input[InputNodes] = {10.0 , 10.0};   //initilize
float _output = 0.0;                        //initilize
float _hidden[HiddenNodes];
float _norm_input[InputNodes];
float _accum;
int x,y,i,j;

void setup() {
  Serial.begin(115200); 
}

void loop() {
  //konversi analog
  //x = analogRead(A0);
  //y = analogRead(A1);
  
  //konversi ke persen kelembaban
  //_input[0] = mapfloat(x, ADCMin, ADCMax, 0.0, 100.0);
  //_input[1] = mapfloat(y, ADCMin, ADCMax, 0.0, 100.0);

  //forward propagation nn
  nn_compute();
  delay(1000);
}

float mapfloat(float x, float in_min, float in_max, float out_min, float out_max){
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

void nn_compute(){
  //input normalization
  for(i=0;i<InputNodes;i++){
    _norm_input[i] = mapfloat(_input[i],InputMin,InputMax,0.0,1.0); 
  }
  //empty memory
  for(i=0;i<HiddenNodes;i++){
    _hidden[i] = 0;
  } 
  //compute hidden layer
  for(i=0;i<HiddenNodes;i++){
    for(j=0;j<InputNodes;j++){
      _hidden[i] += _norm_input[j] * _w1[i][j];
    }
    _hidden[i] = 1.0/(1.0 + exp(-(_hidden[i]+_b1[i])));
  }
  
  //compute output layer
  _accum = 0;
  for(i=0;i<HiddenNodes;i++){
    _accum += _hidden[i] * _w2[i];
  }
  _output = 1.0/(1.0 + exp(-(_accum + _b2)));
  Serial.println(_output);
}
